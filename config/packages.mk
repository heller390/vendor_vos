# VoltageOS packages
PRODUCT_PACKAGES += \
    Covers \
    ThemePicker \
    ThemesStub \
    BatteryStatsViewer \
    GameSpace \
    OmniJaws \
    WeatherIcons \
    ParallelSpace \
    LogViewer \
    Updater

ifeq ($(VOLTAGE_BUILD_TYPE), OFFICIAL)
    PRODUCT_PACKAGES += \
	Updater
endif

# Browser
#PRODUCT_PACKAGES += \
#    Jellyfish

# BtHelper
#PRODUCT_PACKAGES += \
#    BtHelper

# VoltageOS UDFPS animations
ifeq ($(EXTRA_UDFPS_ANIMATIONS),true)
PRODUCT_PACKAGES += \
    UdfpsAnimations
endif

# Extra tools in Voltage
PRODUCT_PACKAGES += \
    awk \
    bzip2 \
    curl \
    getcap \
    libsepol \
    setcap \

# Filesystems tools
PRODUCT_PACKAGES += \
    fsck.exfat \
    mke2fs \
    mkfs.exfat \
